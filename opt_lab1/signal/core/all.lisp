(uiop:define-package :signal/core/all
    (:use :cl
          :signal/core/lexer/all)
  (:nicknames :signal/core
              :sig-core)
  (:export #:format-out-tokens
           #:upload-result-to-file
           #:lexer))
 
