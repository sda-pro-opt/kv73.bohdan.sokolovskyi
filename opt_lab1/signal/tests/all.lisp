(uiop:define-package :signal/tests/all
    (:use :cl
          :signal/tests/lexer)
  (:nicknames :signal/tests
              :sig-tests)
  (:shadow #:test-suite)
  (:export #:test-suite))

(in-package :signal/tests/all)

(defun test-suite ()
  (signal/tests/lexer:test-suite))


